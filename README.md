# COMPX301 Assignment 3

Third group assignment for COMPX301

A regex searcher written in rust, following the specifications
from the [assignment instructions](https://www.cs.waikato.ac.nz/~tcs/COMPX301/assign3-2019.html).

The library is able to search UTF-8 characters, including special/control characters like `\n`

The binary splits input files by `\n`, so you can search by any character other than `\n`

# Terms

 - *regex* - Regular Expression (The pattern)
 - *FSM* - Finite State Machine

# Authors
 - Daniel Martin - 1349779
 - Rex Pan - 1318909

# In This Project

### Binaries

 - `src/REcompiler.rs` *(re_compiler)* - Takes a regex expression as a argument, and
    outputs a description for a FSM.
 - `src/REsearcher.rs` *(re_searcher)* - Takes the output of `re_compiler` and a file
    argument, outputs all the lines in the file that match.

### Libraries

 - `src/lib.rs` - Contains all our multi library tests.
 - `src/dequeue_lib.rs` - Our implementation of a dequeue for the searcher.
 - `src/REcompiler_lib.rs` - Contains the functions which turn a regex string to a FSM.
 - `src/REsearcher_lib.rs` - Contains the functions that take the FSM and search on a string.

# How To Use

This code was compiled during development with the latest stable release of
rust - `rustc 1.34.1 (2019-04-24)`, though it should work with any other version of rust that
supports the 2018 edition.

To compile the binaries (`src/REcompiler.rs` and `src/REsearcher.rs`) use cargo
which will organize dependencies and link them.

```
$cargo build --release
```

The compiled binaries will be in `target/release/`.

It is recommended to pipe the output of `re_compiler` to `re_searcher` so you do not have to
copy paste or use a intermediate file.

### Arguments

`re_compiler` requires a string which is the regex in which it will produce a state machine for.
Make sure to put the expression in quotes (e.g. `"(Hello)|(Goodbye)"`), 
otherwise it would be interpreted as multiple arguments when there is a space.

The definition of a regex can be found in the header of the `src/REcompiler.rs` file.

Optionally you can input a optimisation flag **after** the regex string.
The choices are:
 - `re_compiler "regex" -2`: Full optimisation (default).
 - `re_compiler "regex" -1`: Stable optimisation, keeps the actual matched portion of the string the same, doesn't matter for this assignment.
 - `re_compiler "regex" -0`: No optimisations, improves compilation times (not by that much).

Optimisation shortens the compiled FSM and search times.

`re_searcher` takes a string for the path (relative or absolute) of the file to search through line by line.
Path should also be in quotation marks if there are spaces.

### Example

To search `text.txt` with the regex; `reg*ex` (`regex` with zero to many `g`'s in the middle),
navigate to `target/release/` and run the binaries:

```
$cd target/release/
$./re_compiler "reg*ex" | ./re_searcher text.txt
```

# Output

The output of `re_compiler` is a ascii representation of a finite state machine, this can be
feed straight to `re_searcher`. For more details on the format of the output, look in the header
documentation of `re_compiler`.

`re_searcher` outputs any line that is matched from the input file, according to the state machine
from `re_compiler`.
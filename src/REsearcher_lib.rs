//! The searcher library. (note: `node` == `state`)
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//!
//! # Structs
//!
//!  - `ToFSM` - Takes the FSM character iterator (from `REcompiler`) as input and
//!  provides an iterator over each state of the FSM.
//!  - `FSMSearcher` - Same as FSM in compiler, contains one state (`MatchingChars` and linked states).
//!
//! # Enums
//!
//!  - `MatchingChars` - Defines what characters can be matched in a state.
//!
//! # Functions
//!
//!  - `flatten` - Takes a state number and a fsm, returns a array of the next state/s that
//!  consume a character.
//!  - `search` - Takes a string and a fsm, returns true if there is a match.

use crate::deque_lib::Deque;

/// A struct, contains a char iterator and provides an iterator over states given via the char iterator.
/// Assumes the char iterator is in the format of the output of `src/REcompiler.rs`.
pub struct ToFSM<I: Iterator<Item=char> + Clone> {
	source: I,
}

impl<I: Iterator<Item=char> + Clone> ToFSM<I> {
	pub fn new(source: I) -> Self {
		Self {
			source,
		}
	}
}

impl<I: Iterator<Item=char> + Clone> ToFSM<I> {
	/// Assumes the iterator is at the end of the line (state numbers) and returns
	/// the two state numbers for the current state.
	fn parse_last_digits(&mut self) -> (usize, usize) {
		let mut first_num = String::new();

		// Consume number and space afterwards for state number 1
		for first_num_piece in self.source.by_ref() {
			if first_num_piece == ' ' { break; }
			first_num.push(first_num_piece);
		}

		let mut second_num = String::new();

		// Consume number and space afterwards for state number 2
		for second_num_piece in self.source.by_ref() {
			if second_num_piece == '\n' { break; }
			second_num.push(second_num_piece);
		}

		// Return the two numbers (need to convert string to usize)
		(first_num.parse().expect("Invalid FSM input"), second_num.parse().expect("Invalid FSM input"))
	}

	/// Checks a character is what we think it is.
	/// Panics with a slighly helpful message if it doesn't match
	fn assert_next_char(&mut self, assert_character: char) {
		match self.source.next() {
			Some(new_char) if new_char == assert_character => return,
			Some(new_char) => panic!("Expected a '{}' but found '{}'", assert_character, new_char),
			None => panic!("Expected '{}' but hit end of source", assert_character),
		}
	}
}

impl<I: Iterator<Item=char> + Clone> Iterator for ToFSM<I> {
	type Item = FSMSearcher;

	/// Iterate over the states contained in the char iterator.
	fn next(&mut self) -> Option<FSMSearcher> {
		let mut output = FSMSearcher::default();

		// Consume and throw away the state number
		for new_char in self.source.by_ref() {
			if new_char == ' ' { break; }
		}

		let begin_delimiter = self.source.next()?;

		// Decide on the type that this state will match
		match begin_delimiter {
			'^' => { // Any one literal NOT in the list

				// Check and consume the open bracket
				self.assert_next_char('[');

				let mut matching = Vec::new();

				matching.push(self.source.next().expect("Lists must be at least one char long"));

				for new_char in self.source.by_ref() {
					if new_char == ']' { break; }
					matching.push(new_char);
				}

				// Check and consume the space at the end of the character list
				self.assert_next_char(' ');

				output.matching_chars = MatchingChars::Exclude(matching);
			}
			'[' => { // Any one literal in the list
				let mut matching = Vec::new();

				matching.push(self.source.next().expect("Lists must be at least one char long"));

				for new_char in self.source.by_ref() {
					if new_char == ']' { break; }
					matching.push(new_char);
				}

				// Check and consume the space at the end of the character list
				self.assert_next_char(' ');

				output.matching_chars = MatchingChars::Include(matching);
			}
			'.' => { // Any character
				output.matching_chars = MatchingChars::Wildcard;
				self.assert_next_char(' ');
			}
			' ' => { // Don't consume anything (spaces will be in a list like this `[ ]`)
				output.matching_chars = MatchingChars::Branching;
			}

			// Not a special character, consume the single literal
			literal => {
				output.matching_chars = MatchingChars::Include(vec![literal]);
				self.assert_next_char(' ');
			}
		}

		// Get the next two linked states
		let digits = self.parse_last_digits();
		output.next1 = digits.0;
		output.next2 = digits.1;

		Some(output)
	}
}

/// Specifies what a state can match.
///
///  - `Include` Matches any one character in the array.
///  - `Exclude` Matches any one character not in the array.
///  - `Branching` Matches nothing (consumes nothing).
///  - `Wildcard` Matches any one character.
#[derive(Debug, PartialEq, Eq)]
pub enum MatchingChars {
	Include(Vec<char>),
	Exclude(Vec<char>),
	Branching,
	Wildcard,
}

impl Default for MatchingChars {
	fn default() -> Self {
		MatchingChars::Branching
	}
}

/// A state of a FSM.
///
///  - `matching_chars` - What the state will match, see `MatchingChars`.
///  - `next1` - The first branching state.
///  - `next2` - The second branching state.
#[derive(Debug, PartialEq, Eq, Default)]
pub struct FSMSearcher {
	pub matching_chars: MatchingChars,
	pub next1: usize,
	pub next2: usize,
}

/// Reduces `MatchingChars::Branching` to a array of states that only consume a character.
/// Uses deque as a linked list, sounds way better than Vec in theory, probably slower for most cases in practise
fn flatten(entry: usize, fsm: &[FSMSearcher]) -> Deque<usize> {
	match fsm.get(entry) {
		Some(c) if c.matching_chars == MatchingChars::Branching => {
			if c.next1 == c.next2 { // Turning on optimise during compile time should eliminate any cases of c.next1 == c.next2
				flatten(c.next1, fsm)
			} else {
				flatten(c.next1, fsm).concat(flatten(c.next2, fsm))
			}
		}
		// Entry is `None` or isn't a branching state
		_ => {
			let mut deque = Deque::new();
			deque.push_back(entry);
			deque
		}
	}
}

/// Search a string using the given FSM.
/// Returns `true` if the fsm matches any part of the string, otherwise `false`.
pub fn search(searchee: &str, fsm: &[FSMSearcher]) -> bool {
	use std::collections::HashSet;
	// The "Back" of the queue is the current_state, the front is the next state
	let mut deque: Deque<Option<usize>> = Deque::new();
	// Stores duplications that are already in the queue.
	let mut dup_checks = [HashSet::new(), HashSet::new()];
	let (already_in_current_queue, already_in_next_queue) = dup_checks.split_at_mut(1);
	let (mut already_in_current_queue, mut already_in_next_queue) = (&mut already_in_current_queue[0], &mut already_in_next_queue[0]);
	// Let None be the SCAN value used in the deque

	// Flatten once here and store the result,
	// Otherwise this is being called for every character in the string in the worst case.
	//
	// NOTE: We could actually cache the flattened result of every branch state, or even have the compiler
	// pre-compute all this for us, which would improve performance by quite a bit, but that removes the need for a deque
	// and performance is already pretty decent, so we didn't.
	let mut start_state_flatten = flatten(0, fsm);
	let mut start_states = Vec::with_capacity(start_state_flatten.len());

	// Change the start states to a vec (faster iteration as we need to do it for every character in the searchee)
	while let Some(idx) = start_state_flatten.pop_back() {
		if idx >= fsm.len() { return true; } // The regex is trivial, so just return true.
		// If there is dup in the start_states, remove it before putting it into the array that will be inserted multiple times.
		if already_in_current_queue.insert(idx) {
			start_states.push(idx);
		}
	}
	// Clears the queue, memory should be still allocated.
	already_in_current_queue.clear();

	// Create a char array through the searchee string
	let mut searchee_chars = searchee.chars();

	// Loop for each character in the searchee. If we hit the end of the string, we know it's not a match.
	while let Some(candi) = searchee_chars.next() {

		// Add all the start states
		for idx in &start_states {
			// If a duplicate doesn't already exist in the queue. (start_states has no dups within itself)
			if !already_in_next_queue.contains(idx) {
				deque.push_front(Some(*idx));
			}
		}

		// Add SCAN value
		deque.push_front(None);

		// Clear for the old queue
		already_in_current_queue.clear();
		// Swap the references around, put the current queue into old queue, no data moved.
		std::mem::swap(&mut already_in_current_queue, &mut already_in_next_queue);

		// Loop to the SCAN value
		while let Some(node_idx) = deque.pop_back().unwrap() {
			let node = &fsm[node_idx];

			// A closure (inline function) to reduce code duplication
			let mut processor = || {
				// Add next node 1 and also add 2 if it's not the same

				if node.next1 >= fsm.len() { return true; } // We have found the end node
				// If a duplicate doesn't already exist in the queue.
				if already_in_next_queue.insert(node.next1) {
					deque.push_front(Some(node.next1));
				}

				if node.next1 != node.next2 {
					if node.next2 >= fsm.len() { return true; } // We have found the end node
					// If a duplicate doesn't already exist in the queue.
					if already_in_next_queue.insert(node.next2) {
						deque.push_front(Some(node.next2));
					}
				}
				false
			};

			// Check if the current node matches the current char in the searchee, if not, push next node onto deque
			// Returns true if we have reached the end
			if match node.matching_chars {
				MatchingChars::Include(ref to_match) if to_match.contains(&candi) => processor(),

				MatchingChars::Exclude(ref to_not_match) if !to_not_match.contains(&candi) => processor(),

				MatchingChars::Wildcard => processor(), // A wildcard matches everything

				MatchingChars::Branching => {
					if node.next1 >= fsm.len() { return true; } // We have found the end node
					if node.next2 >= fsm.len() { return true; } // We have found the end node

					// If a duplicate doesn't already exist in the queue.
					if already_in_current_queue.insert(node.next1) {
						// Push onto current queue
						deque.push_back(Some(node.next1));
					}

					// If a duplicate doesn't already exist in the queue.
					if already_in_current_queue.insert(node.next2) {
						// Push onto current queue
						deque.push_back(Some(node.next2));
					}
					false
				}
				_ => false
			} { return true; }
		}
	}
	false
}

//! This library tests both the compiler and the searcher.
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909

#![allow(non_snake_case)]

pub mod REcompiler_lib;
pub mod REsearcher_lib;
pub mod deque_lib;

#[cfg(test)]
mod test {
	// Get all public things because we will test them all
	use crate::{REcompiler_lib::*, REsearcher_lib::*};

	#[test]
	fn memo_test() {
		let fsm = compile_helper("aa*b");
		assert!(!search("aaa", &fsm));
	}

	#[test]
	fn last_elem() {
		let fsm = compile_helper("c");
		assert!(search("bbbbbbbbbc", &fsm));
	}
	
	#[test]
	fn star_question_stuff() {
		let fsm = compile_helper("a*22dd*g?s");
		assert!(!search("a*22dd*g?s", &fsm));
		assert!( search("22ds", &fsm));
		assert!( search("aaaaaa22dddddddgsaass", &fsm));
		assert!( search("aabb22ddgssd", &fsm));
		assert!( search("aaaaaa222ddssfdds", &fsm));
		assert!( search("aaaaaaaaaaaaaaaaaaaaaaaaaaa22dddddddddddddddddddds", &fsm));
		assert!( search("aaaaaaaaaa22ddds", &fsm));
		assert!(!search("ddddsd2d2dd2", &fsm));
		assert!(!search("aaaaaaaaaa21ds", &fsm));
	}

    #[test]
	fn backslash_test() {
		let fsm = compile_helper(r#"\\\\\\\\\\"#);
		assert!(search(r#"aa\\\\\\\\\\aa"#, &fsm));
		assert!(search(r#"\\\\\\\\\\"#, &fsm));
		assert!(search(r#"aa\\\\\\aa"#, &fsm));
		assert!(!search(r#"\\\\"#, &fsm));
	}

    #[test]
    fn wildcard_star_stack_test() {
		let fsm = compile_helper(r#".*.*.*.*"#);
		assert!(search(r#"asdweafagagfd"#, &fsm));
		assert!(search(r#""#, &fsm));
		assert!(search(r#"!!!!!!"#, &fsm));
		assert!(search(r#".*"#, &fsm));
	}
	#[test]
	fn escape_test() {
		let fsm = compile_helper(r#"\?\*(\?\?)*\["#);
		assert!(!search(r#"\?\?"#, &fsm));
		assert!(search(r#"?*["#, &fsm));
		assert!(!search(r#"?*?["#, &fsm));
		assert!(search(r#"?*??["#, &fsm));
	}
	
	#[test]
	fn question_stack_test() {
		let fsm = compile_helper(r#"a?b?\\?"#);
		assert!(search(r#"asdweafagagfd"#, &fsm));
		assert!(search(r#""#, &fsm));
		assert!(search(r#"!!!!!!"#, &fsm));
		assert!(search(r#"\\"#, &fsm));
	}
	
	#[test]
	fn bracket_question_test() {
		let fsm = compile_helper(r#"ss(drive)?a?vv"#);
		assert!(search(r#"ssvv"#, &fsm));
		assert!(search(r#"ssssvvvv"#, &fsm));
		assert!(!search(r#"sssdvvv"#, &fsm));
		assert!(!search(r#"ssdrivv"#, &fsm));
		assert!(search(r#"svsvsvssdrivevv"#, &fsm));
		assert!(search(r#"ssdriveavv"#, &fsm));
		assert!(!search(r#"ssdrivedriveavv"#, &fsm));
		assert!(!search(r#"ssdriveaavv"#, &fsm));
	}
	
	#[test]
	fn bracket_star_test() {
		let fsm = compile_helper(r#"(red)(red)*vv*"#);
		assert!(search(r#"redvvv"#, &fsm));
		assert!(!search(r#"reddvv"#, &fsm));
		assert!(!search(r#"redd"#, &fsm));
		assert!(!search(r#"reddvv"#, &fsm));
		assert!(search(r#"redredv"#, &fsm));
		assert!(search(r#"derf drefredredvvvvvvvvv"#, &fsm));
	}

	#[test]
	fn trivial_test() {
		let fsm = compile_helper("a");
		assert!(search("a", &fsm));
		assert!(!search("e", &fsm));

		let fsm = compile_helper("abcceee");
		assert!(search("abcceee", &fsm));
		assert!(!search("abceee", &fsm));
	}

	#[test]
	fn contains_within_test() {
		let fsm = compile_helper("abcceee");
		assert!(search("dsfasdfwjoabcceeeassxc", &fsm));
		assert!(search("abccabcceeeeee", &fsm));
		assert!(!search("dsfwedsfabceeeefsdf", &fsm));
	}

	#[test]
	fn literal_brackets() {
		let fsm = compile_helper("[abc]");
		assert!(!search("]", &fsm));
		assert!(!search("[", &fsm));
		assert!(search("c", &fsm));

		let fsm = compile_helper("a[abc]");
		assert!(search("bcabdefg", &fsm));
		assert!(search("ac", &fsm));
		assert!(!search("adb", &fsm));

		let fsm = compile_helper("a[a\\bc\\]");
		assert!(search("a\\", &fsm));
		assert!(search("ac", &fsm));
		assert!(!search("adb", &fsm));
	}

	#[test]
	fn parentheses_and_brackets() {
		let fsm = compile_helper("([abc][abc][abc]?)([abc][abc][abc]?)*dd");
		assert!(search("abcdd", &fsm));
		assert!(!search("avavadd", &fsm));
		assert!(search("aadd", &fsm));
		assert!(search("aaaadd", &fsm));
		assert!(search("ddaaaaadd", &fsm));
		assert!(search("ddacbcadd", &fsm));
	}

	#[test]
	fn negation_brackets() {
		let fsm = compile_helper("^[abc]");
		assert!(search("]", &fsm));
		assert!(search("[", &fsm));
		assert!(search("^", &fsm));
		assert!(!search("c", &fsm));
		assert!(search("d", &fsm));

		let fsm = compile_helper("a^[]abc]");
		assert!(!search("a", &fsm));
		assert!(!search("a]", &fsm));
		assert!(!search("aa", &fsm));
		assert!(search("aft", &fsm));
	}

	#[test]
	fn odd_characters() {
		let fsm = compile_helper("[Cc][Cc]*.*📐📐*💤📺👾📦");
		assert!(search("CCCurabitur 🎿🍵🐥🎻📒🍅📊💸📐💤📺👾📦🐧🕂🍩🍴🎬 🐧📴🍝🎳🌗🐨", &fsm));
		assert!(search("ccCCC 🎿🍵🐥🎻📒🍅📊💸📐📐📐📐📐📐📐📐📐💤📺👾📦🐧🕂🍩🍴🎬 🐧📴🍝🎳🌗🐨", &fsm));
		assert!(!search("ccCCC 🎿🍵🐥🎻📒🍅📊💸💤📺👾📦🐧🕂🍩🍴🎬 🐧📴🍝🎳🌗🐨", &fsm));
	}

	fn compile_helper(regex: &str) -> Vec<FSMSearcher> {
		let out = dump_fsm(&compile(regex, OptimiseLevel::All).unwrap());
		ToFSM::new(out.chars()).collect::<Vec<FSMSearcher>>()
	}
}


//! Deque data structure, output restricted. Used by the searcher to organise states.
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909

use std::fmt::Debug;
use std::rc::Rc;
use std::cell::RefCell;

/// A double ended queue, output restricted using a linking method (nodes are linked, not in a looped list)
/// 
/// You can only remove items from the back of the list.
/// 
/// Functions are fairly self explanatory, just linking and unlinking nodes with some special cases.
#[derive(Debug)]
pub struct Deque<D: Debug> {
	head: Option<Rc<RefCell<DequeNode<D>>>>,
	tail: Option<Rc<RefCell<DequeNode<D>>>>,
	length: usize,
}

impl<D: Debug> Deque<D> {
	pub fn new() -> Deque<D> {
		Deque { head: None, tail: None, length: 0 }
	}

	pub fn len(&self) -> usize {
		self.length
	}

	pub fn push_back(&mut self, new_data: D) {
		self.head = Some(Rc::new(RefCell::new(DequeNode { data: new_data, next: self.head.take() })));
		if self.length == 0 {
			self.tail = self.head.clone(); // Clones the reference (should)
		}
		self.length += 1;
	}

	pub fn push_front(&mut self, new_data: D) {
		if self.length == 0 {
			self.push_back(new_data);
		} else {
			let new_node = Some(Rc::new(RefCell::new(DequeNode { data: new_data, next: None })));
			self.tail.take().unwrap().borrow_mut().next = new_node.clone();
			self.tail = new_node;
			self.length += 1;
		}
	}

	pub fn pop_back(&mut self) -> Option<D> {
		match self.length {
			0 => None,
			1 => {
				self.length -= 1;
				self.tail = None; // Drop the additional reference to the same element so that we can retrieve it in the following line
				let node = Rc::try_unwrap(self.head.take().unwrap()).unwrap().into_inner();
				Some(node.data)
			}
			_ => {
				self.length -= 1;
				let node = Rc::try_unwrap(self.head.take().unwrap()).unwrap().into_inner();
				self.head = node.next;
				Some(node.data)
			}
		}
	}

	pub fn concat(self, other: Self) -> Deque<D> {
		if self.length == 0 { return other; }
		if other.length == 0 { return self; }

		self.tail.unwrap().borrow_mut().next = other.head;
		Deque { head: self.head, tail: other.tail, length: self.length + other.length }
	}
}

/// A node of the linked deque.
#[derive(Debug)]
struct DequeNode<D: Debug> {
	data: D,
	next: Option<Rc<RefCell<DequeNode<D>>>>,
}

#[cfg(test)]
mod test {
	use crate::deque_lib::Deque;

	#[test]
	fn queue_usage_test() {
		let mut deq = Deque::new();
		deq.push_front(1);
		deq.push_front(2);
		deq.push_front(3);
		deq.push_front(4);
		deq.push_front(5);

		assert_eq!(deq.pop_back(), Some(1));
		assert_eq!(deq.pop_back(), Some(2));
		assert_eq!(deq.pop_back(), Some(3));
		assert_eq!(deq.pop_back(), Some(4));
		assert_eq!(deq.pop_back(), Some(5));
		assert_eq!(deq.pop_back(), None);
	}

	#[test]
	fn stack_usage_test() {
		let mut deq = Deque::new();
		deq.push_back(1);
		deq.push_back(2);
		deq.push_back(3);
		deq.push_back(4);
		deq.push_back(5);

		assert_eq!(deq.pop_back(), Some(5));
		assert_eq!(deq.pop_back(), Some(4));
		assert_eq!(deq.pop_back(), Some(3));
		assert_eq!(deq.pop_back(), Some(2));
		assert_eq!(deq.pop_back(), Some(1));
		assert_eq!(deq.pop_back(), None);
	}

	#[test]
	fn deque_usage_test() {
		let mut deq = Deque::new();
		deq.push_back(1);
		deq.push_front(2);
		deq.push_back(3);
		deq.push_front(4);
		deq.push_back(5);

		assert_eq!(deq.pop_back(), Some(5));
		assert_eq!(deq.pop_back(), Some(3));
		assert_eq!(deq.pop_back(), Some(1));
		assert_eq!(deq.pop_back(), Some(2));
		assert_eq!(deq.pop_back(), Some(4));
		assert_eq!(deq.pop_back(), None);
	}

	#[test]
	fn deque_not_much_stuff_test() {
		let mut deq = Deque::new();
		deq.push_back(1);
		assert_eq!(deq.pop_back(), Some(1));
		deq.push_front(2);
		assert_eq!(deq.pop_back(), Some(2));
		assert_eq!(deq.pop_back(), None);
	}
}
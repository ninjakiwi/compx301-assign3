//! The searcher library. (note: `node` == `state`)
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//!
//! Takes a state machine via stdin defined by `src/REcompiler.rs` and a file to match against.
//! Outputs each line in the file that was matched.

extern crate custom_regex;

use std::io::{Read, BufRead};
use custom_regex::REsearcher_lib::*;

fn main() {
	let input = {
		let mut input = String::new();
		std::io::stdin().read_to_string(&mut input).expect("Expected FSM via stdio, encountered error trying to read");
		input
	};

	// Create the fsm
	let fsm = ToFSM::new(input.chars()).collect::<Vec<FSMSearcher>>();

	// Search the file line by line
	for line in std::io::BufReader::new(
		std::fs::File::open(std::env::args().nth(1).expect("Expect file name as argument, got nothing"))
			.expect("Cannot open file at given path.")).lines()
		.map(|c| c.expect("Failed to read line from file.")) {

		// Output any lines that match
		if search(&line, &fsm) {
			println!("{}", line);
		};
	};
}
//! This is the library for the compiler.
//! 
//! For the regex grammar definition, see `src/REcompiler.rs`.
//! For the FSM output grammar, look at the `dump_fsm` function in this file.
//! 
//! To construct the FSM, `expression` is called which will also call `term`, `factor` which could also call `expression` again.
//! So `expression`, `term` and `factor` are used for FSM construction, and `optimise_fsm` and `remove_state` are used for optimisations.
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//!
//! ## Enums:
//!
//!  - `OptimiseLevel` - Levels of optimisation the compiler can do.
//!  - `RegexParsingErrors` - Errors that can be encountered while parsing a regex.
//! 
//! ## Structs:
//!
//!  - `FSM` - A node for a finite state machine.
//!
//! ## Functions:
//!
//!  - `compile` (public) - Takes a regex and `OptimiseLevel` as input, returns a finite state machine as a vector of `FSM`s.
//!  - `dump_fsm` (public) - Returns a string from a FSM containing the finite state machine, one state per line.
//!  - `expression` - Takes a char iterator (from the regex) and a `FSM` vector, adds to the FSM vector for the regex until the end of the string or a `)`.
//!  - `term` - Takes a char iterator and a `FSM` vector, adds a term from the regex to the fsm.
//!  - `factor` - Takes a char iterator and a `FSM` vector, adds a factor from the regex to the fsm.
//!  - `is_vocab` - Takes a character, and checks if it has no special meaning.
//!  - `optimise_fsm` - Takes a `FSM` vector and optimises it.
//!  - `remove_state` - Takes a `FSM` vector, a state to remove and the state old references should now goto, removes the specified state.

use std::iter::Peekable;
use std::error::Error;
use std::cmp;
use core::fmt;

/// Specifies the intensity of finite state machine optimisation
///
/// **Options**
///  - `Off` - Skip the optimiser completely.
///  - `All` - Optimise assuming we just want a t/f match (This removes optional start and end states).
///  - `Stable` - Same as `All` but leaves optional start and end characters, maintaining actual matched text output as `Off`
#[derive(PartialEq)]
pub enum OptimiseLevel {
	Off,
	All,
	Stable,
}

/// A node structure for the finite state machine.
/// 
/// **Variables**
///  - `current_char` - The character/s that this state consumes
///  - `next1` - A possible next state after this state.
///  - `next2` - A possible next state after this state.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FSM {
	current_char: String,
	next1: usize,
	next2: usize,
}

/// Possible errors we can encounter while parsing the regex input.
#[derive(Debug, PartialEq)]
pub enum RegexParsingErrors {
	/// Invalid Regex Index
	RegexInvalid(usize),
	EndedTooEarly,
	/// Opening bracket index
	NoClosingBracket(usize),
}

impl fmt::Display for RegexParsingErrors {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			RegexParsingErrors::RegexInvalid(idx) => {
				write!(f, "The regex syntax is invalid at character: {}", idx)
			}
			RegexParsingErrors::NoClosingBracket(idx) => {
				write!(f, "There is no closing bracket for the opening bracket at position: {}", idx)
			}
			RegexParsingErrors::EndedTooEarly => {
				write!(f, "There is no more input to parse, but more input was expected")
			}
		}
	}
}

impl Error for RegexParsingErrors {}

/// Takes a regex (see format in `REcompiler.rs`) and optimiser flag and returns a fsm.
pub fn compile(input_regex: &str, optimise: OptimiseLevel) -> Result<Vec<FSM>, RegexParsingErrors> {
	let mut fsm = Vec::new();

	// Push the start state (empty so we can make it a splitter)
	fsm.push(FSM {
		current_char: String::new(),
		next1: 1,
		next2: 1,
	});

	let mut regex_iterator = input_regex.chars().enumerate().peekable();

	expression(&mut regex_iterator, &mut fsm)?;

	// If expression exited without consuming the string; unmatched brackets, return err
	if let Some((idx, _val)) = regex_iterator.next() {
		return Err(RegexParsingErrors::RegexInvalid(idx));
	}

	optimise_fsm(&mut fsm, optimise);

	Ok(fsm)
}

/// Returns a full string representation of the fsm (one state per line).
/// If a state refers to a state larger than the largest state, the state
/// machine can end that branch because a match has been found.
///
/// ## Format
///
/// Each line consists of these values, separated by spaces:
///
///  - **State** - The number which is that state, always in order (0 to n states).
///  - **Characters** - Can come in a few different formats: (a space must be contained in list brackets (`[ ]`))
///      - `.` - This means any character.
///      - **list** - A list of literals, if it comes after a `^` it means *Not* any of the characters in the list.
///      - **literal** - just a single character. (`.` is any character and `^` or `[` are invalid inputs)
///      - **nothing** - if missing, this is a node that consumes no characters.
///  - **State 1** - A state which can be branched to from this state.
///  - **State 2** - A state which can be branched to from this state.
/// 
/// ## Example
/// 
/// A state machine that consumes a `a` then `b` would look like this:
/// 
/// 0 a 1 1
/// 1 b 2 2
///
pub fn dump_fsm(fsm: &[FSM]) -> String {
	let mut output = String::new();
	for (count, item) in fsm.iter().enumerate() {
		output.push_str(&format!("{} {} {} {}\n", count, item.current_char, item.next1, item.next2));
	}
	output
}

/// Appends an expression to the `fsm` using the `input_string` iterator.
fn expression<I: Iterator<Item=(usize, char)>>(input_string: &mut Peekable<I>, fsm: &mut Vec<FSM>) -> Result<(), RegexParsingErrors> {

	// This gets the current last state (won't be current anymore after the term fn)
	let start_state = fsm.len() - 1;

	term(input_string, fsm)?;

	loop {

		// At this point the most recent action was consuming a term

		// Do the action for the current char (very likely to consume characters)
		match input_string.peek() {

			// Continue adding terms if they are there
			Some((_idx, '[')) | Some((_idx, '^')) | Some((_idx, '(')) | Some((_idx, '\\')) | Some((_idx, '.')) => { term(input_string, fsm)?; }
			Some((_idx, nc)) if is_vocab(*nc) => { term(input_string, fsm)?; }

			// A closing bracket! we need to return back to the factor fn
			Some((_idx, ')')) => return Ok(()),


			// We have hit the holy splitter
			Some((_idx, '|')) => {
				input_string.next(); // Consume '|'

				// Save pos of start of current expression
				let exp_start = fsm[start_state].next2;

				// Set start of the previous expression to link to the next expression we're about to make
				// Set both if they are the same but if a splitter than only the 2nd one (because it's pointing to the future)
				if fsm[start_state].next1 == fsm[start_state].next2 {
					fsm[start_state].next1 = fsm.len();
				}
				fsm[start_state].next2 = fsm.len();

				let last_state_before_or = fsm.len() - 1;

				// consume a char [done] and add one to fsm len (create the splitter)
				fsm.push(FSM {
					current_char: String::new(), // consume nothing
					next1: exp_start, // This is where we link back to the first expression
					next2: fsm.len() + 1, // Set to next term, will be replaced if wrong
				});

				// Get the term on the other side of the "|" (this consumes the remaining expression)
				expression(input_string, fsm)?;

				if fsm[last_state_before_or].next1 == fsm[last_state_before_or].next2 {
					fsm[last_state_before_or].next1 = fsm.len();
				}
				fsm[last_state_before_or].next2 = fsm.len();


				// This here, is the butter which melts together complicated things.
				// (joins outputs into one node, but don't bother if there are no more inputs)
				if input_string.peek().is_some() {
					fsm.push(FSM {
						current_char: String::new(), // consume nothing
						next1: fsm.len() + 1,
						next2: fsm.len() + 1,
					});
				}

				// The remaining part of the expression was done as the other half of the "|"
				// so we must be done with this expression and can return
				return Ok(());
			}

			// Hit a (non vocab / invalid functionality) character at the start of an expression (bad input)
			Some((idx, _next_char)) => {
				return Err(RegexParsingErrors::RegexInvalid(*idx));
			}

			// Hit end of input, finished!
			None => {
				return Ok(());
			}
		}
	}
}

/// Appends a term to the `fsm` using the `input_string` iterator.
fn term<I: Iterator<Item=(usize, char)>>(input_string: &mut Peekable<I>, fsm: &mut Vec<FSM>) -> Result<(), RegexParsingErrors> {
	let start_state = fsm.len() - 1;

	factor(input_string, fsm)?;

	// Check for next value, if doesn't exist then just return nicely
	let next_char = match input_string.peek() {
		Some((_idx, next_char)) => *next_char,
		_ => return Ok(())
	};


	if next_char == '*' {
		input_string.next(); // Consume '*' (should be safe because it was checked)

		// Save pos of start of just created factor
		let exp_start = fsm[start_state].next2;

		// Set start of the previous expression to link to the next expression we're about to make
		// Set both if they are the same but if a splitter than only the 2nd one (because it's pointing to the future)
		if fsm[start_state].next1 == fsm[start_state].next2 {
			fsm[start_state].next1 = fsm.len();
		}
		fsm[start_state].next2 = fsm.len();

		// Make the factor (just obtained) points to the splitter already because it is right after (not made yet)

		// consume a char [done] and add one to fsm len (create the splitter)
		fsm.push(FSM {
			current_char: String::new(), // consume nothing
			next1: exp_start, // This is where we link back to the first term
			next2: fsm.len() + 1, // Set to next term
		});
	} else if next_char == '?' {
		input_string.next(); // Consume '?' (should be safe because it was checked)

		// Save pos of start of just created factor
		let exp_start = fsm[start_state].next2;

		// Set start of the previous expression to link to the next expression we're about to make
		// Set both if they are the same but if a splitter than only the 2nd one (because it's pointing to the future)
		if fsm[start_state].next1 == fsm[start_state].next2 {
			fsm[start_state].next1 = fsm.len();
		}
		fsm[start_state].next2 = fsm.len();

		// Make the factor (just obtained) point to the end node rather than the splitter
		let prev_factor = fsm.len() - 1;
		if fsm[prev_factor].next1 == fsm[prev_factor].next2 {
			fsm[prev_factor].next1 = fsm.len() + 1;
		}
		fsm[prev_factor].next2 = fsm.len() + 1;

		// consume a char [done] and add one to fsm len (create the splitter)
		fsm.push(FSM {
			current_char: String::new(), // consume nothing
			next1: exp_start, // This is where we link back to the first term
			next2: fsm.len() + 1, // Set to next term
		});

		// add one to fsm len (create the end node)
		fsm.push(FSM {
			current_char: String::new(), // consume nothing
			next1: fsm.len() + 1, // Set both to next term
			next2: fsm.len() + 1,
		});
	}
	Ok(())
}

/// Appends a factor to the `fsm` using the `input_string` iterator.
fn factor<I: Iterator<Item=(usize, char)>>(input_string: &mut Peekable<I>, fsm: &mut Vec<FSM>) -> Result<(), RegexParsingErrors> {

	// Consume next char because we will use it
	match input_string.next() {
		Some((_idx, ' ')) => { // Needed because space is parsed as a separator in the searcher. Have to be before is_vocab match.
			fsm.push(FSM {
				current_char: "[ ]".to_string(),
				next1: fsm.len() + 1,
				next2: fsm.len() + 1,
			});
			Ok(())
		}

		Some((_idx, next_char)) if is_vocab(next_char) || next_char == '.' => {
			fsm.push(FSM {
				current_char: next_char.to_string(),
				next1: fsm.len() + 1,
				next2: fsm.len() + 1,
			});
			Ok(())
		}

		Some((_idx, '\\')) => {
			// Get the next character as a literal whatever it is, return early with err if nothing
			let next_literal = match input_string.next() {
				Some((_idx, new_char)) => format!("[{}]", new_char),
				None => return Err(RegexParsingErrors::EndedTooEarly), //Expected some literal character, but hit end
			};

			fsm.push(FSM {
				current_char: next_literal,
				next1: fsm.len() + 1,
				next2: fsm.len() + 1,
			});
			Ok(())
		}

		Some((opening_bracket_idx, '(')) => {

			// Just check it isn't a "()" or string ends
			match input_string.peek() {
				Some((idx, new_char)) if *new_char == ')' => return Err(RegexParsingErrors::RegexInvalid(*idx)), // If empty brackets, invalid regex
				Some(_) => expression(input_string, fsm)?,
				None => return Err(RegexParsingErrors::NoClosingBracket(opening_bracket_idx)), // Open bracket but hit end of string (not closed)
			}

			// Consume the closing bracket
			match input_string.next() {
				Some((_idx, new_char)) if new_char == ')' => Ok(()),
				None => Err(RegexParsingErrors::NoClosingBracket(opening_bracket_idx)), // Expected close parenthesis but hit end
				Some(_) => unreachable!(), // Shouldn't ever happen, expression should only ever exit on the end of string or a ')'
			}
		}

		Some((idx, multi)) if multi == '^' || multi == '[' => {
			let negative = multi == '^';
			if negative && input_string.next().ok_or(RegexParsingErrors::EndedTooEarly)?.1 != '[' {
				return Err(RegexParsingErrors::RegexInvalid(idx + 1)); // Expected open bracket but didn't get it.
			}
			let mut output = if negative { "^[".to_owned() } else { "[".to_owned() };

			output.push(input_string.next().ok_or(RegexParsingErrors::EndedTooEarly)?.1);

			// Loop till we reach closing bracket, or return Err if we hit end without doing so.
			loop {
				match input_string.next() {
					Some((_idx, new_char)) if new_char == ']' => break,
					Some((_idx, new_char)) => output.push(new_char),
					None => return Err(RegexParsingErrors::NoClosingBracket(if negative { idx + 1 } else { idx })),
				}
			}

			output.push(']');

			fsm.push(FSM {
				current_char: output,
				next1: fsm.len() + 1,
				next2: fsm.len() + 1,
			});
			Ok(())
		}

		Some((idx, _)) => { // An unexpected character
			Err(RegexParsingErrors::RegexInvalid(idx))
		}

		// End of string
		None => Err(RegexParsingErrors::EndedTooEarly),
	}
}

/// Returns true if the `char` is not a special character.
fn is_vocab(candi: char) -> bool {
	match candi {
		'[' | ']' | '.' | '*' | '?' | '|' | '(' | ')' | '^' | '\\' => { false }
		_ => true
	}
}

/// Optimises the fsm
///
/// ## Room For Improvement
///  - Remove repeats of the same thing in sequence (`ab*b*a` = `ab*a`)
///  - Shorten half duplicates (`abc|abcd` = `abcd?` or even `abcx|abcy` = `abc(x|y)` also for the start of a or as well)
///
/// ## Optimisations:
///  - **Stage 1** - Check if start state has no characters and if it is not a branching machine then swaps it with the target (2nd) state and deletes itself
///  - **Stage 2** - Remove optional starting literals
///  - **Stage 3** - Check for states that only have one reference, and if the referrer state has no char, then delete the useless state
///  - **Stage 4** - Check there are no branching machines that consume no chars and can go to output (because that means it already matched)
///  - **Stage 5** - Remove repeats
///  - **Stage 6** - Remove unused states (can be created by previous step)
///
/// ## Arguments
///  - **fsm** - The Finite State Machine to optimise
///  - **optimise_level** - Allow the optimiser to change the matched string (removes optional start and end characters),
///    does not change the result if you are just looking for a t/f if it matched or not.
fn optimise_fsm(fsm: &mut Vec<FSM>, optimise_level: OptimiseLevel) {
	// Skip if optimise is off and don't bother optimise a fsm already as small as one can be
	if optimise_level == OptimiseLevel::Off || fsm.len() <= 1 { return; }

	// Loop until no more changes are made to the fsm (finite state machine)
	loop {
		let fsm_start_size = fsm.len();

		// ## Stage 1
		//
		// Separate from stage 2 because we need to keep the entry point at the first node)
		// While the first state is useless...
		while fsm[0].current_char == "" && fsm[0].next1 == fsm[0].next2 && fsm[0].next1 < fsm.len() {
			let second_thing_idx = fsm[0].next1;

			fsm.swap(0, second_thing_idx);

			remove_state(fsm, second_thing_idx, 0);
		}


		// ## Stage 2
		// ### Remove optional starting sections
		// Skip this stage if the start can finish straight away because it's sorted in stage 4,
		// or skip if the first state consumes a character.
		if optimise_level == OptimiseLevel::All {
			let larger_pointer = cmp::max(fsm[0].next1, fsm[0].next2);
			if larger_pointer != fsm.len() && fsm[0].current_char == "" {
				let smaller_pointer = cmp::min(fsm[0].next1, fsm[0].next2);
				// larger and smaller pointer are the two states from the start state

				// end_pointer is the smallest of itself and it's two pointers (should all be below the smaller pointer)
				let end_pointer = cmp::min(larger_pointer, cmp::min(fsm[larger_pointer].next1, fsm[larger_pointer].next2));

				// check all the nodes from the small state to the end state that they don't link to past the end state
				let mut can_remove = smaller_pointer != larger_pointer;
				for state in fsm.iter().take(end_pointer).skip(smaller_pointer) {
					if !can_remove { break; }
					if state.next1 > larger_pointer || state.next2 > larger_pointer {
						can_remove = false;
					}
				}

				// If no states branch past the end state, we can remove it as it is an optional start.
				if can_remove {
					// Put larger_pointer to start, delete the old start (and move references too)
					fsm.swap(0, larger_pointer);
					remove_state(fsm, larger_pointer, 0);
				}
			}
		}


		// ## Stage 3
		// ### Remove pointer nodes
		// States that consume no characters, and are not a branching machine
		for (count, state) in fsm.iter().enumerate() {
			if count == 0 { // Don't remove the start node (sorted out earlier)
				continue;
			}

			// If a pointer node with no char; remove
			if state.next1 == state.next2 && state.current_char == "" {
				remove_state(fsm, count, state.next1);
				break;
			}
		}


		// ## Stage 4
		// ### Remove end loops
		// Any splitters the contain an end state are useless as the pattern can match straight away
		//
		// >	**Example** "ab*" can be shortened to "a".
		if optimise_level == OptimiseLevel::All {
			for state in 0..fsm.len() {
				// If we can end, then end right away
				if fsm[state].next1 == fsm.len() || fsm[state].next2 == fsm.len() {
					// Set both references to the end
					fsm[state].next1 = fsm.len();
					fsm[state].next2 = fsm.len();
				}
			}
		}


		// ## Stage 5
		// ### Remove repeats
		// Where state number is different but state itself is the same
		//
		// >	**Example:** "a|a" is the same as "a".
		'outer: for (state1, stateobj1) in fsm.clone().iter().enumerate() {
			for state2 in state1 + 1..fsm.len() {
				if *stateobj1 == fsm[state2] {
					remove_state(fsm, state2, state1);
					break 'outer;
				}
			}
		}


		// ## Stage 6
		// ### Remove unused states (can be created by other stages)
		// >	**Example:**
		// >		"0 a 2 2"
		// >		"1 b 2 2"
		// >	State 1 is never used, so can be removed
		let mut has_ref = vec!(false; fsm.len());
		has_ref[0] = true; // Mark start node as used

		// Loop until all states that you can get to through state 0 are marked in `has_ref`
		loop {
			let start_ref = has_ref.clone();

			// For each state
			for (i, state) in fsm.iter().enumerate() {
				// If current state is referenced then we know the next states are referenced
				if has_ref[i] {
					if state.next1 < fsm.len() { has_ref[state.next1] = true; }
					if state.next2 < fsm.len() { has_ref[state.next2] = true; }
				}
			}

			// List not changed so we must have found all the linked states
			if start_ref == has_ref { break; }
		}
		// Go through each state and check if it was never referenced so we can remove it
		for (i, tf_ref) in has_ref.iter().enumerate() {
			// Remove the state if it was never used
			if !(*tf_ref) {
				// replace state number doesn't matter as all other states referencing the state will be deleted
				// because they must also be impossible to get to from the start state
				remove_state(fsm, i, 0);
				break;
			}
		}


		// Repeat until no changes are done or the length is down to 1
		if fsm.len() == fsm_start_size { break; }
		if fsm.len() <= 1 { break; }
	}
}

fn remove_state(fsm: &mut Vec<FSM>, state_num: usize, new_land_state: usize) {
	// Don't try to remove a non existing state (end state)
	if state_num >= fsm.len() { return; }

	// if the new state is larger than the deleted one, subtract 1 so it targets the intended state
	let new_land_state = if new_land_state >= state_num { new_land_state - 1 } else { new_land_state };

	fsm.remove(state_num);

	// Now fix up all the state references for states on or after the deleted state
	for state in fsm {
		state.next1 = match state.next1.cmp(&state_num) {
			cmp::Ordering::Less => state.next1,
			cmp::Ordering::Greater => state.next1 - 1,
			cmp::Ordering::Equal => new_land_state,
		};

		state.next2 = match state.next2.cmp(&state_num) {
			cmp::Ordering::Less => state.next2,
			cmp::Ordering::Greater => state.next2 - 1,
			cmp::Ordering::Equal => new_land_state,
		};
	}
}

#[cfg(test)]
mod test {
	use super::{compile, OptimiseLevel, FSM, RegexParsingErrors};

	#[test]
	fn trivial_literals() {
		let out_fsm = compile("abcd", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_owned(), next1: 1, next2: 1 }, FSM { current_char: "a".to_owned(), next1: 2, next2: 2 }, FSM { current_char: "b".to_owned().to_owned(), next1: 3, next2: 3 }, FSM { current_char: "c".to_owned(), next1: 4, next2: 4 }, FSM { current_char: "d".to_owned(), next1: 5, next2: 5 }]);

		let out_fsm = compile("ffddsfdgsfgifififii33iifsgijeiejgfdkgjdfiieigjdjddd242342", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_owned(), next1: 1, next2: 1 }, FSM { current_char: "f".to_owned(), next1: 2, next2: 2 }, FSM { current_char: "f".to_owned(), next1: 3, next2: 3 }, FSM { current_char: "d".to_owned(), next1: 4, next2: 4 }, FSM { current_char: "d".to_owned(), next1: 5, next2: 5 }, FSM { current_char: "s".to_owned(), next1: 6, next2: 6 }, FSM { current_char: "f".to_owned(), next1: 7, next2: 7 }, FSM { current_char: "d".to_owned(), next1: 8, next2: 8 }, FSM { current_char: "g".to_owned(), next1: 9, next2: 9 }, FSM { current_char: "s".to_owned(), next1: 10, next2: 10 }, FSM { current_char: "f".to_owned(), next1: 11, next2: 11 }, FSM { current_char: "g".to_owned(), next1: 12, next2: 12 }, FSM { current_char: "i".to_owned(), next1: 13, next2: 13 }, FSM { current_char: "f".to_owned(), next1: 14, next2: 14 }, FSM { current_char: "i".to_owned(), next1: 15, next2: 15 }, FSM { current_char: "f".to_owned(), next1: 16, next2: 16 }, FSM { current_char: "i".to_owned(), next1: 17, next2: 17 }, FSM { current_char: "f".to_owned(), next1: 18, next2: 18 }, FSM { current_char: "i".to_owned(), next1: 19, next2: 19 }, FSM { current_char: "i".to_owned(), next1: 20, next2: 20 }, FSM { current_char: "3".to_owned(), next1: 21, next2: 21 }, FSM { current_char: "3".to_owned(), next1: 22, next2: 22 }, FSM { current_char: "i".to_owned(), next1: 23, next2: 23 }, FSM { current_char: "i".to_owned(), next1: 24, next2: 24 }, FSM { current_char: "f".to_owned(), next1: 25, next2: 25 }, FSM { current_char: "s".to_owned(), next1: 26, next2: 26 }, FSM { current_char: "g".to_owned(), next1: 27, next2: 27 }, FSM { current_char: "i".to_owned(), next1: 28, next2: 28 }, FSM { current_char: "j".to_owned(), next1: 29, next2: 29 }, FSM { current_char: "e".to_owned(), next1: 30, next2: 30 }, FSM { current_char: "i".to_owned(), next1: 31, next2: 31 }, FSM { current_char: "e".to_owned(), next1: 32, next2: 32 }, FSM { current_char: "j".to_owned(), next1: 33, next2: 33 }, FSM { current_char: "g".to_owned(), next1: 34, next2: 34 }, FSM { current_char: "f".to_owned(), next1: 35, next2: 35 }, FSM { current_char: "d".to_owned(), next1: 36, next2: 36 }, FSM { current_char: "k".to_owned(), next1: 37, next2: 37 }, FSM { current_char: "g".to_owned(), next1: 38, next2: 38 }, FSM { current_char: "j".to_owned(), next1: 39, next2: 39 }, FSM { current_char: "d".to_owned(), next1: 40, next2: 40 }, FSM { current_char: "f".to_owned(), next1: 41, next2: 41 }, FSM { current_char: "i".to_owned(), next1: 42, next2: 42 }, FSM { current_char: "i".to_owned(), next1: 43, next2: 43 }, FSM { current_char: "e".to_owned(), next1: 44, next2: 44 }, FSM { current_char: "i".to_owned(), next1: 45, next2: 45 }, FSM { current_char: "g".to_owned(), next1: 46, next2: 46 }, FSM { current_char: "j".to_owned(), next1: 47, next2: 47 }, FSM { current_char: "d".to_owned(), next1: 48, next2: 48 }, FSM { current_char: "j".to_owned(), next1: 49, next2: 49 }, FSM { current_char: "d".to_owned(), next1: 50, next2: 50 }, FSM { current_char: "d".to_owned(), next1: 51, next2: 51 }, FSM { current_char: "d".to_owned(), next1: 52, next2: 52 }, FSM { current_char: "2".to_owned(), next1: 53, next2: 53 }, FSM { current_char: "4".to_owned(), next1: 54, next2: 54 }, FSM { current_char: "2".to_owned(), next1: 55, next2: 55 }, FSM { current_char: "3".to_owned(), next1: 56, next2: 56 }, FSM { current_char: "4".to_owned(), next1: 57, next2: 57 }, FSM { current_char: "2".to_owned(), next1: 58, next2: 58 }]);
	}

	#[test]
	fn wildcards() {
		let out_fsm = compile("he*llo", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_owned(), next1: 1, next2: 1 }, FSM { current_char: "h".to_owned(), next1: 3, next2: 3 }, FSM { current_char: "e".to_owned(), next1: 3, next2: 3 }, FSM { current_char: "".to_owned(), next1: 2, next2: 4 }, FSM { current_char: "l".to_owned(), next1: 5, next2: 5 }, FSM { current_char: "l".to_owned(), next1: 6, next2: 6 }, FSM { current_char: "o".to_owned(), next1: 7, next2: 7 }]);

		assert_eq!(compile("h**ello", OptimiseLevel::Off), Err(RegexParsingErrors::RegexInvalid(2)));
		assert_eq!(compile("(he)**ello", OptimiseLevel::Off), Err(RegexParsingErrors::RegexInvalid(5)));

		let out_fsm = compile("h*e*l*", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_owned(), next1: 2, next2: 2 }, FSM { current_char: "h".to_owned(), next1: 2, next2: 2 }, FSM { current_char: "".to_owned(), next1: 1, next2: 4 }, FSM { current_char: "e".to_owned(), next1: 4, next2: 4 }, FSM { current_char: "".to_owned(), next1: 3, next2: 6 }, FSM { current_char: "l".to_owned(), next1: 6, next2: 6 }, FSM { current_char: "".to_owned(), next1: 5, next2: 7 }]);
	}

	#[test]
	fn brackets() {
		// Check empty brackets (invalid because the expression does not exist)
		let out_fsm = compile("()", OptimiseLevel::Off);
		assert_eq!(out_fsm, Err(RegexParsingErrors::RegexInvalid(1)));

		// Check unequal brackets
		assert_eq!(compile("a)", OptimiseLevel::Off), Err(RegexParsingErrors::RegexInvalid(1)));

		// Check more unequal brackets
		assert_eq!(compile("(a", OptimiseLevel::Off), Err(RegexParsingErrors::NoClosingBracket(0)));
	}

	#[test]
	fn brackets_and_or() {
		let out_fsm = compile("(aa|cc|dd)|bb", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_owned(), next1: 11, next2: 11 }, FSM { current_char: "a".to_owned(), next1: 2, next2: 2 }, FSM { current_char: "a".to_owned(), next1: 10, next2: 10 }, FSM { current_char: "".to_owned(), next1: 1, next2: 6 }, FSM { current_char: "c".to_owned(), next1: 5, next2: 5 }, FSM { current_char: "c".to_owned(), next1: 9, next2: 9 }, FSM { current_char: "".to_owned(), next1: 4, next2: 7 }, FSM { current_char: "d".to_owned(), next1: 8, next2: 8 }, FSM { current_char: "d".to_owned(), next1: 9, next2: 9 }, FSM { current_char: "".to_owned(), next1: 10, next2: 10 }, FSM { current_char: "".to_owned(), next1: 14, next2: 14 }, FSM { current_char: "".to_owned(), next1: 3, next2: 12 }, FSM { current_char: "b".to_owned(), next1: 13, next2: 13 }, FSM { current_char: "b".to_owned(), next1: 14, next2: 14 }]);

		assert_eq!(compile("h||ello", OptimiseLevel::Off), Err(RegexParsingErrors::RegexInvalid(2)));
	}

	#[test]
	fn square_brackets() {
		// Check empty brackets (should crash as ] is a literal if first)
		// Slightly cryptic error message
		assert_eq!(compile("[]", OptimiseLevel::Off), Err(RegexParsingErrors::NoClosingBracket(0)));

		// Close bracket with no open
		assert_eq!(compile("hi]lo", OptimiseLevel::Off), Err(RegexParsingErrors::RegexInvalid(2)));

		// Check for abc
		let out_fsm = compile("[abc]", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_string(), next1: 1, next2: 1 }, FSM { current_char: "[abc]".to_string(), next1: 2, next2: 2 }]);

		// Check brackets for ] and a
		let out_fsm = compile("[]a]", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_string(), next1: 1, next2: 1 }, FSM { current_char: "[]a]".to_string(), next1: 2, next2: 2 }]);

		// Check brackets for not ] and a
		let out_fsm = compile("^[]b]", OptimiseLevel::Off).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "".to_string(), next1: 1, next2: 1 }, FSM { current_char: "^[]b]".to_string(), next1: 2, next2: 2 }]);
	}

	#[test]
	fn simple_optimiser() {
		// Simple start and end test
		let out_fsm = compile("a*bc*", OptimiseLevel::All).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "b".to_string(), next1: 1, next2: 1 }]);

		// Simple duplicate test
		let out_fsm = compile("a|a", OptimiseLevel::All).unwrap();
		assert_eq!(out_fsm, vec![FSM { current_char: "a".to_string(), next1: 1, next2: 1 }]);
	}
}

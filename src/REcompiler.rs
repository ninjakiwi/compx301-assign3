//! The compiler's main function.
//!
//! # Authors
//!  - Daniel Martin - 1349779
//!  - Rex Pan - 1318909
//!
//! # Grammar Format:
//! 
//! Remember if you want a special character as a literal, you must precede it with a `\`.
//! 
//! The compiler follows the strict rules below, where your input regex is `E`.
//! Although the rules are well defined below, I will outline some common questions: (note all these examples are pointless in practice)
//! 
//! #### Is `a**` valid?
//! 
//! No, the special literal `*` is only ever in the compiler rules on this line `T -> F*`. Although we have `F -> (E)` which means you can have `(a*)*`.
//! (Same goes for `?`)
//! 
//! #### Is `()` valid?
//! 
//! No, because of `F -> (E)`, there must be an expression inside, which there is not.
//! 
//! #### Is `]` valid?
//! 
//! No (They are the closing bracket for a list of characters), to get a `]`, you must precede it with a `\`.
//! 
//!
//! ## Special Characters:
//!
//! - `.` - One character (any character).
//! - `\` - This character will be ignored but whatever character is after it will be consumed as a literal.
//! - `*` - Means the previous factor can be consumed 0 to many times.
//! - `?` - Means the previous factor can be consumed 0 to 1 times.
//! - `[` - States the start of a list of literals, only one of the literals will be consumed (even special characters will be literals)
//! - `]` - Specifies the end of a list of literals, will be consumed as a literal if first in the list.
//! - `^` - Not operator, use preceding a list of literals (`^[abc]`).
//!
//! ## Compiler Rules:
//!
//! `l` = Every character
//! `L` = Non special literals (most characters)
//!
//! ### Expressions (`E`)
//!
//! `E` -> `T`
//! `E` -> `TE`
//! `E` -> `T|T`
//!
//! ### Terms (`T`)
//!
//! `T` -> `F`
//! `T` -> `F*`
//! `T` -> `F?`
//!
//! ### List of characters (`I`)
//!
//! `I` -> `l`
//! `I` -> `lI`
//!
//! ### Factors (`F`)
//!
//! `F` -> `.`
//! `F` -> `L`
//! `F` -> `\l`
//! `F` -> `[I]`
//! `F` -> `^[I]`
//! `F` -> `(E)`
//!

extern crate custom_regex;

use custom_regex::REcompiler_lib;
use custom_regex::REcompiler_lib::OptimiseLevel;
use custom_regex::REcompiler_lib::RegexParsingErrors;

fn main() {

	// Get the input regex and the optimisation level if given
	let input_regex = std::env::args().nth(1).expect("Expected regex string, got nothing");

	let optimisation_level =
		match std::env::args().nth(2).as_ref().map(|c| c.as_str()) {
			Some("-2") | None => OptimiseLevel::All,
			Some("-1") => OptimiseLevel::Stable,
			Some("-0") => OptimiseLevel::Off,
			_ => {
				eprintln!("Invalid second argument, maybe forgot quotation marks around regex? Or put optimisation level before regex?");
				return;
			}
		};

	// Run the compiler on the input regex and match the result
	match REcompiler_lib::compile(&input_regex, optimisation_level) {

		// All is good print output and finish
		Ok(fsm) => print!("{}", REcompiler_lib::dump_fsm(&fsm)),

		// The regex was not vaild, print a handy error message
		Err(err) => match err {
			RegexParsingErrors::RegexInvalid(idx) => {
				eprintln!("The input regex is invalid, the '{}' character was unexpected at index number {}.\n", &input_regex[idx..idx + 1], idx);
				point_at_invalid_regex(&input_regex, idx, "This character was unexpected ");
			}
			RegexParsingErrors::NoClosingBracket(idx) => {
				eprintln!("The input regex contains an open bracket '{}' at index number {} without a matching closing bracket.\n", &input_regex[idx..idx + 1], idx);
				point_at_invalid_regex(&input_regex, idx, "This bracket has no closing bracket ");
			},
			RegexParsingErrors::EndedTooEarly => {
				eprintln!("The input regex ended while we still expected more regex to parse");
			}
		}
	}
}

/// Prints a message pointing to the specified index of the given string.
fn point_at_invalid_regex(regex: &str, error_idx: usize, error_msg: &str) {
	eprintln!("Input regex: \"{}\"", regex);
	eprintln!("{1: >0$}", 15 + error_idx, '^');
	eprintln!("{1: >0$}", 15 + error_idx, '|');

	if error_msg.len() > 15 + error_idx {
		eprintln!("{space: >0$}--- {1:}", 15 + error_idx, error_msg, space = '\\')
	} else {
		eprintln!("{err:-<0$}/", 15 + error_idx - 1, err = error_msg)
	}

}
